#ifndef UNICODE
    #define UNICODE
#endif

#include <GLFW/glfw3.h>

#ifdef __has_include
    #if __has_include(<windows.h>)
        #include <windows.h>
        #if defined __WIN32__ || defined __WIN64__
        #endif
    #endif
    #if __has_include(<print>)
        #include <print>
    #endif
#else  // If version does not exist we are working with C++17 or earlier, or bad configuration of path.
    using std::cout;
#endif

#include <cstdlib>
#include <iostream>
#include <exception>

using std::print, std::println, std::cin, std::uint32_t, std::runtime_error, std::exception; // NOLINT

UINT StartConsole(FILE* f)
{
    AllocConsole();
#ifdef UNICODE
    // TODO: Check if unicode font is installed and in registry.
    UINT oldcp = GetConsoleOutputCP();
    SetConsoleOutputCP(CP_UTF8);
    _wfreopen_s(&f, L"CONIN$", L"r", stdin);
    _wfreopen_s(&f, L"CONOUT$", L"w", stdout);
    _wfreopen_s(&f, L"CONOUT$", L"w", stderr);
    return oldcp;
#else
        freopen_s(&f, "CONIN$", "r", stdin);
        freopen_s(&f, "CONOUT$", "w", stdout);
        freopen_s(&f, "CONOUT$", "w", stderr);
        return 0;
#endif
}

void ExitConsole(FILE* f, UINT oldcp)
{
    println("Press Enter to Exit...");
    cin.sync();
    cin.get();
    fclose(f);// NOLINT
#if defined(UNICODE) && (defined(__WIN32__) || defined(__WIN64__))
    SetConsoleOutputCP(oldcp);
#endif
    FreeConsole();
}

constexpr uint32_t const WINDOW_WIDTH{800}, WINDOW_HEIGHT{600};

int WINAPI wWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, PWSTR pCmdLine, int nShowCmd)  // NOLINT
{
    FILE* f {};
    UINT  oldcp = StartConsole(f);
    int exit_code {EXIT_SUCCESS};
    GLFWwindow* pwindow{};
    
    println("Initializing GLFW Window...");
    glfwInit();

    pwindow = glfwCreateWindow(WINDOW_WIDTH, WINDOW_HEIGHT, "Game", nullptr, nullptr);
    if (_pwindow == nullptr)
        throw runtime_error("Failed to initialize GLFW Window.");

    try
    {
        while (!static_cast<bool>(glfwWindowShouldClose(pwindow)))
        {
            glfwPollEvents();
        }
    }
    catch (exception const& e)
    {
        println(stderr, "Exception occurred: {}", e.what());
        exit_code = EXIT_FAILURE;
        // system("PAUSE");
        // ExitConsole(f);
        // return EXIT_FAILURE;
    }

    println("Destroying GLFW Window...");
    if (pwindow != nullptr)
        glfwDestroyWindow(pwindow);

    println("Terminating GLFW...\n");
    glfwTerminate();

    ExitConsole(f, oldcp);

    return exit_code;
}