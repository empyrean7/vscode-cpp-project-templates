# #{projectname} Information

---

- Example information about your project.

## Premake5 Build System Documentation

---
<https://premake.github.io/docs/>
    - Build system used in the project.

## Lua Language Documentation

---
<https://www.lua.org/manual/5.3/manual.html>
    - Scripting language used for configuring premake build system.

## Lua Language Server Documentation

---
.luarc.json settings information: <https://github.com/LuaLS/lua-language-server/wiki/Settings>
    - Live language server used to assist in writing syntactically correct Lua.
