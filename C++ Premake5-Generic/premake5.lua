APPLICATION_NAME = "#{projectname}"

DEFAULTS = {
    workspace_prefix = "wks_",
    project_prefix = "prj_",
    configuration_prefix = "cfg_",
    architecture_prefix = "arc_",
    start_project = APPLICATION_NAME,
    configuration = "Release",
    architecture = "x86_64",
    system = "windows",
}
DEFAULTS.workspace = DEFAULTS.workspace_prefix .. APPLICATION_NAME

function GetCurrentWorkingDirectory()
    return path.getdirectory("")
end

function GetProjectBinDirectory()
    local working_dir = GetCurrentWorkingDirectory()
    return string.format("%s/bin", working_dir)
end

function GetProjectBuildDirectory()
    local working_dir = GetCurrentWorkingDirectory()
    return string.format("%s/build",working_dir)
end

function GetRunApplication()
    local p = premake
    local run_application_name = ""
    for wks in p.global.eachWorkspace() do
        if wks.name == DEFAULTS.workspace then
            for prj in p.workspace.eachproject(wks) do
                if prj.name == DEFAULTS.start_project then
                    run_application_name = prj.name
                    goto label_default_start_project_match
                end
            end
        end
    end
    ::label_default_start_project_match::
    return run_application_name
end

function GetPathToTarget(target_build)
    local bin_dir = GetProjectBinDirectory()
    local relative_path_to_target = string.format("%s/%s/%s-%s/%s/", bin_dir, target_build, DEFAULTS.system, DEFAULTS.architecture, DEFAULTS.start_project)
    local path_to_target = path.getabsolute(relative_path_to_target)
    return path_to_target
end

function GetPathToEXE(target_build)
    return GetPathToTarget(target_build) .. "/" .. DEFAULTS.start_project .. ".exe"
end

function CopyAssets(target_build)
    local path_to_target = GetPathToTarget(target_build)
    local path_to_assets_target = path_to_target .. "/assets"
    if not os.isdir(path_to_assets_target) then
        os.mkdir(path_to_assets_target)
    end

    cmd = os.translateCommands("{COPYDIR} ./assets " .. path_to_assets_target)
    os.executef(cmd)
end

function CopyShaders(target_build)
    local path_to_target = GetPathToTarget(target_build)
    local path_to_shaders_target = path_to_target .. "/shaders"
    if not os.isdir(path_to_shaders_target) then
        os.mkdir(path_to_shaders_target)
    end

    cmd = os.translateCommands("{COPYDIR} ./shaders " .. path_to_shaders_target)
    os.executef(cmd)
end

workspace(DEFAULTS.workspace)
    architecture "x64"
    startproject "#{projectname}"

    toolset "#{c++compiler}"
    toolchainversion "5.0"

    configurations
    {
        "Debug",
        "Release",
        "Minimal-Debug",
        "Fast-Release",
        "Aggressive-Release"
    }

    platforms
    {
        "Windows",
        "Linux",
        "Mac"
    }

    local outputdir = "%{cfg.buildcfg}/%{cfg.system}-%{cfg.architecture}"

project "#{projectname}"
    kind "ConsoleApp"
    language "C++"
    targetdir("bin/" .. outputdir .. "/%{prj.name}")
    objdir("build/" .. outputdir .. "/%{prj.name}")

    local logpath = "%{wks.location}/log/%{prj.name}"

    buildlog(logpath)

    files
    {
        "%{prj.name}/source/**.hpp",
        "%{prj.name}/source/**.cpp"
    }

    includedirs
    {
        "%{prj.name}/source",
    }

    libdirs
    {
        "C:/msys64/clang64/bin",
        "C:/msys64/clang64/lib"
    }

    links
    {
    }

    filter "platforms:Windows"
        system "windows"
        systemversion "latest"

    filter "platforms:Linux"
        system "linux"

    filter "platforms:Mac"
        system "macosx"

    filter "configurations:Debug"
        defines "DEBUG"
        runtime "Debug"
        symbols "on"
        optimize "off"

    filter "configurations:Release"
        defines
        {
            "RELEASE",
            "NDEBUG"
        }
        runtime "Release"
        optimize "on"

    filter "configurations:Minimal-Debug"
        defines "DEBUG"
        runtime "Debug"
        buildoptions
        {
            "-O1"
        }

    filter "configurations:Fast-Release"
        defines
        {
            "RELEASE",
            "NDEBUG"
        }
        runtime "Release"
        buildoptions
        {
            "-O3"
        }

    filter "configurations:Aggressive-Release"
        defines
        {
            "RELEASE",
            "NDEBUG"
        }
        runtime "Release"
        buildoptions
        {
            "-Ofast"
        }

    filter { "system:windows", "action:gmake2" }
        buildoptions {
            "-v",
            "-Wall",
            "-Wextra",
            "-std=#{c++standard}",
        }
        linkoptions
        {
            "-v",
            "-stdlib=#{c++library}",
            "-rtlib=compiler-rt",
            "-std=#{c++standard}",
        }

    newaction {
        trigger = "pclean",
        description = "Clean project object intermediary and binary files.",
        execute = function ()
            working_dir = path.getdirectory("")
            build_dir = string.format("%s/build",working_dir)
            bin_dir = string.format("%s/bin", working_dir)

            print("\nStarting Cleaning Process\n-------------------------\n")
            if os.isdir(build_dir) then
                printf("Build directory found at: %s\nCleaning object files...\n", build_dir)
                os.rmdir(build_dir)
            else
                printf("Build directory not found at: %s\nObjects will not be cleaned...\n", build_dir)
            end

            if os.isdir(bin_dir) then
                printf("Binary directory found at: %s\nCleaning binary files...\n", bin_dir)
                os.rmdir(bin_dir)
            else
                printf("Binary directory not found at: %s\nBinaries will not be cleaned...\n", bin_dir)
            end

            print("Cleaning Makefiles...")
            if os.isfile("./Makefile") then
                os.remove("./Makefile")
            end

            if os.isfile("./#{projectname}.make") then
                os.remove("./#{projectname}.make")
            end

            print("Remaking Directory Structure...")
            local p = premake
            local tmpdirstring = ""
            os.mkdir("bin")
            os.mkdir("build")
            for wks in p.global.eachWorkspace() do
                for prj in p.workspace.eachproject(wks) do
                    for cfg in p.project.eachconfig(prj) do
                        tmpdirstring = cfg.buildcfg .. "/" .. cfg.system .. "-" .. cfg.architecture
                        os.mkdir("bin/" .. cfg.buildcfg)
                        os.mkdir("bin/" .. tmpdirstring)
                        os.mkdir("build/" .. cfg.buildcfg)
                        os.mkdir("build/" .. tmpdirstring)
                    end
                end
            end
        end
    }

    newaction {
        trigger = "prun",
        description = "Run the startup project application from the bin directory.",
        execute = function ()

            if not os.isfile("./Makefile") then
                print("\nGenerating Makefile...\n")
                cmd = "premake5 pclean"
                os.executef(cmd)
                cmd = "premake5 gmake2"
                os.executef(cmd)
            end

            local path_to_exe = GetPathToEXE(DEFAULTS.configuration)
            local cmd = "start " .. path_to_exe
            print(cmd)
            os.executef(cmd)
        end
    }

    newaction {
        trigger = "pmrun",
        description = "Make the startup project then run the built application from the bin directory.",
        execute = function ()
            local path_to_exe = GetPathToEXE("Release")

            local cmd = ""
            print("\nGenerating Makefile...\n")
            cmd = "premake5 pclean"
            os.executef(cmd)
            cmd = "premake5 gmake2"
            os.executef(cmd)

            if not os.isfile(path_to_exe) then
                cmd = "make config=" .. string.lower(DEFAULTS.configuration) .. "_" ..  string.lower(DEFAULTS.system)
                print("\nBuilding application with command: " .. cmd .. "\n")
                os.executef(cmd)
            end

            if os.isfile(path_to_exe) then
                cmd = "start " .. path_to_exe
                print("\nExecuting application with command: " .. cmd .."\n")
                os.executef(cmd)
            else
                print("\nCannot execute application, build process failed...\n")
            end
        end
    }

    newaction {
        trigger = "pmdrun",
        description = "Make the startup project in debug then run the built application from the bin directory.",
        execute = function ()
            local path_to_target = GetPathToTarget("Debug")
            local path_to_exe = GetPathToEXE("Debug")
            print("\nBuilding to file path: " .. path_to_exe .. "\n")

            local cmd = ""
            print("Generating Makefile...\n")
            cmd = "premake5 pclean"
            os.executef(cmd)
            cmd = "premake5 gmake2"
            os.executef(cmd)

            print("\nCopying Assets...\n")
            CopyAssets("Debug")

            if not os.isfile(path_to_exe) then
                cmd = "make config=debug_" ..  string.lower(DEFAULTS.system)
                print("\nBuilding application with command: " .. cmd .. "\n")
                os.executef(cmd)
            end

            if os.isfile(path_to_exe) then
                cmd = "start " .. path_to_exe
                print("\nExecuting application with command: " .. cmd .. "\n")
                os.executef(cmd)
            else
                print("\nCannot execute application, build process failed...\n")
            end
        end
    }

    newaction {
        trigger = "pmbd",
        description = "Make the startup project in debug and build the application.",
        execute = function ()
            local path_to_exe = GetPathToEXE("Debug")

            local cmd = ""
            print("Generating Makefile...")
            cmd = "premake5 pclean"
            os.executef(cmd)
            cmd = "premake5 gmake2"
            os.executef(cmd)

            if not os.isfile(path_to_exe) then
                cmd = "make config=debug_" ..  string.lower(DEFAULTS.system)
                print("Building application with command: " .. cmd)
                os.executef(cmd)
            end
        end
    }

    newaction {
        trigger = "pmbr",
        description = "Make the startup project in release and build the application.",
        execute = function ()
            local path_to_exe = GetPathToEXE("Release")

            local cmd = ""
            cmd = "premake5 pclean"
            os.executef(cmd)
            print("Generating Makefile...")
            cmd = "premake5 gmake2"
            os.executef(cmd)

            if not os.isfile(path_to_exe) then
                cmd = "make config=release_" ..  string.lower(DEFAULTS.system)
                print("Building application with command: " .. cmd)
                os.executef(cmd)
            end
        end
    }

    newaction {
        trigger = "pcopyd",
        description = "Copy assets and shaders to Debug target directory.",
        execute = function ()
            CopyAssets("Debug")
        end
    }

    newaction {
        trigger = "pcopyr",
        description = "Copy assets and shaders to Release target directory.",
        execute = function ()
            CopyAssets("Release")
        end
    }