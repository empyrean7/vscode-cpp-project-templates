#include <cstdlib>
#include <iostream>

#ifdef __has_include
#if __has_include(<print>)  // Check if we are working in C++20 or later.
#include <print>
#endif
#else  // If version does not exist we are working with C++17 or earlier, or bad configuration of path.
using std::cout;
#endif

using std::print, std::println, std::cin;

int ExitConsole()
{
    println("Press Enter To Exit...");
    cin.sync();
    cin.get();
    return EXIT_SUCCESS;
}

int main(int argc, char** argv)  // NOLINT
{
    println("Hello World");
    return ExitConsole();
}